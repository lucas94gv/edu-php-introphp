# README #

### Para qué es este repo? ###

Este repo pretende una introducción rápida a la sintaxis de php y plantea unos ejercicios sencillos como práctica.

Como requisitos previos se asumen nociones básicas de programación, concretamente este es un buen ejercicio si se parte de un paradigma de programación estructurada.


### Lo que se pretende aprender de este ejercicio ###

* Sintaxis básica de PHP
* Declaración de variables
* Aritmética
* Lógica
* Estructuras de control
* Bucles
* Impresión
* Estructuras de datos
* Empleo de funciones
* Envío de datos desde formularios
* Manejo de sesiones
* Paso por valor o paso por referencia



## Antes de ponerte a trabajar...

##### Haz un fork del repositorio original ####
Haz un fork del repositorio original y **configúralo de forma privada** (la actividad propuesta es individual o en grupo cerrado de alumnos según se indique ;)
Habilita las issues e indica que es un proyecto PHP.

Invita al profesor con permiso de escritura (Write).

##### Clona el repositorio en tu entorno de trabajo####
```Shell
git clone <url de tu fork>
```

##### Importa el proyecto en el IDE ####
* Utilizaremos Atom por el momento


#### Tu rama de revisión o tu rama principal de trabajo ####
Tu solución actual (más reciente) deberá estar apuntada por tu rama master. Puedes utilizar todas las ramas que quieras, pero asegúrate, si tienes otras ramas que forman parte de tu solución, de combinarlas con tu rama master para incluirlas en la revisión. Dicho de otra forma, esta rama será tu rama de integración.

Recuerda la forma de crear una nueva rama:

```Shell
git checkout -b <nombreRama>
```

###Documenta tu trabajo###

El repo debe contener una carpeta nombrada como `doc`. [Sigue las instrucciones](doc/README.md) de cómo documentar.

##Cuándo termines tu trabajo... o eso crees...##

##### Etiqueta tu versión ####

Cuando tengas un revisión de tu código que consideres estable, etiquétala de la forma que te indique el [mecanismo de versionado](doc/README.md). Modifica tambien el [changelog](doc/changelog.md) indicando las novedades de la versión.
Puedes hacer etiquetado de tu último commit de la siguiente manera:

```
# Si quieres hacer una etiqueta ligera (solo nombrar un commit
git tag <etiqueta>

# Si quieres hacer una etiqueta que contenga más información
git tag -a <etiqueta> -m 'El mensaje'
```

Si quieres poner una etiqueta a un commit anterior, pon su checksum al final de las instrucciones anteriores.

Recuerda enviar tus tags a tus repos remotos de la siguiente manera:

```
git push <remoto> <tag>
```

Consulta esta [fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado) para más detalles.

#### Crea una issue en bitbucket ####

Para que se haga una revisión de tu código. Crea o reabre la incidencia (issue de tipo tag asignada al profesor) cada vez que tengas una versión principal (v1, v2, v3, etc.) o la version que tengas previa a la demo si trabajas en el marco de Scrum. Crea la issue de la siguiente manera:

* De título, indica: `Revisión: <nombreRepo - usuario/equipo>`
* En la descripción haz todas las observaciones que consideres pertinentes al profesor. Indícale tu versión y pon el código hash del commit.
* Asígnala al profesor
* Pon el tipo `task`
* Asigna la prioridad dependiendo de la urgencia de revisión. Sólo si la revisión bloquea tu trabajo y no puedes continuar, pon prioridad `blocker`
* Puedes adjuntar pantallas u otros ficheros si fuesen necesarios para la revisión.

Cuando el profesor te indique que debes realizar alguna acción para que continue la revisión, éste pondrá la issue en estado `on hold` (en espera).

Cuando quieras que continúe la revisión, si has hecho algún cambio (por pequeño que sea) debes elevar versión y reabrir la issue modificando el workflow a `open`. Recuerda que esto, por agilizar, se puede hacer [automáticamente y directamente con el commit](https://confluence.atlassian.com/bitbucket/resolve-issues-automatically-when-users-push-code-221451126.html) de la siguiente manera:

`git commit -m "Está totalmente adaptado a PEP8, reopening #6"`

De esta forma bitbucket modificará automáticamente el workflow y comentará la issue con el nº de commit donde lo resuelves.



### Guía de Contribution ###

* Escribe test
* Revisa el código
* Other guidelines

### Who do I talk to? ###

Siguiendo este orden de preferencia:

* Issue Tracking
* agrasar@iessanclemente.net
 
