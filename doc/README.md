#Instrucciones de documentación#

La primera consideración es que **NO debes editar ningún fichero de README.md para tu propia documentación**. Si quieres corregir algún error ortográfico o de cualquier tipo, realízalo a través de un pull request.

Para documentar tu código debes contemplar los siguientes aspectos:

## Mecanismo de versionado ##

En la resolución del primer ejercicio utiliza este sistema de versionado:

**Importante:** Sé estricto y preciso formando la cadena de la versión. Utiliza caracteres [ASCII](https://es.wikipedia.org/wiki/ASCII) (sin acentos). Pon tu nombre con la primera letra en minúscula siguiendo el resto con el estilo [lowerCamelCase](https://es.wikipedia.org/wiki/CamelCase).

```Shell
# Ejemplo
git tag e01-v1.0-juanCarlosDeBorbon
```

* `e01-v1.x-nombreApellido1` donde `x` representa un número secuencial en cada incremento.
* `e02-v1.x.y-nombreApellido1` donde `x` representa un número secuencial en evolución o cambio funcional (o corrección de error) de tu App e `y` (opcional), representa un cambio no funcional debido a una refactorización de código, introducción de doc, etc...
* `e03-v1.x.y-nombreApellido1` donde `x` representa un número secuencial en evolución o cambio funcional (o corrección de error) de tu App e `y` (opcional), representa un cambio no funcional debido a una refactorización de código, introducción de doc, etc...