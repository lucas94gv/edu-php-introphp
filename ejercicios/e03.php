<?php session_start(); ?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Ejercicio 03</title>
	<style>
		.debug{background-color:lightpink;}
	</style>
</head>

<body>
	<div id="enunciado" style="background-color: lightgray;">
	<h1>Enunciado</h1>
	<p>Crea un html con un formulario en el que aparezcan los siguientes campos:</p>
	<ul>
		<li>Nombre</li>
		<li>Apellido</li>
		<li>Edad</li>
		<li>Nota</li>
	</ul>
	<p>Al enviar este formulario al servidor, se deben cumplir los siguientes requerimientos:</p>
	<ol>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Hay cinco alumnos PRECARGADOS que han sacado diferentes calificaciones a lo largo del curso en diferentes tareas asignadas:
			<ul>
				<li>Eugenio Martínez de 45 años - Notas de tareas: 7, 6, 5, 8, 5, 6, 9, 10</li>
				<li>Marta Carrera de 22 - Notas de tareas: 1, 6, 2, 3, 5, 6, 9, 10, 10, 9</li>
				<li>Nacho Herrera de 25 - Notas de tareas: 3, 4, 2, 4, 6, 7, 9, 10, 3, 7</li>
				<li>Anxo Iglesias de 32 - Notas de tareas: 1, 6, 2, 3'2, 5, 2'2, 4'7, 5'5, 9, 9</li>
				<li>Valentina Iglesias de 30 - Notas de tareas: 9, 7</li>
			</ul>
		</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>La estructura de datos elegida es un array indexado por las claves nombre, apellido, edad, notas para cada alumno. Y un array que agrupe estos alumnos</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Es condición necesaria que toda esta estructura estea referenciada por una única variable para que pueda ser almacenada fácilmente en una variable de sesión</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Haz una lista que enumere los alumnos y sus notas medias.</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Calcula notas medias en función de los rangos de edad [18,29], [30,39], [más de 40]</li>
		<li><strong>(Reutilizado del ejercicio anterior): </strong>Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos</li>
		<li>Todos los datos son obligatorios y ninguno debe de enviarse vacío.</li>
		<li>La estructura de datos debe almacenarse en la sesión del usuario que realiza las peticiones.</li>
		<li>Si no hay ningún alumno coincidente en nombre y apellido, se añadirá este a la estructura de datos.</li>
		<li>Si hay un alumno coincidente en nombre y apellido, se añadirá a este alumno, la nueva nota enviada por formulario.</li>
		<li>Debe existir un botón en el formulario con el texto 'Matar Sesion' que termine con la sesión y consecuentemente con sus datos.</li>
	</ol>
	<p>Es necesario que esta sea entregada en plazo para la evaluación</p>
	</div>

	<h1>Resultado</h1>

		<form method="POST">
			<p>Nombre: <input type="text" name="nombre"></p>
			<p>Apellido: <input type="text" name="apellido"></p>
			<p>Edad: <input type="number" name="edad"></p>
			<p>Nota: <input type="number" name="nota"></p>
			<p><input type="submit" name="enviar" value="Enviar"></p>
			<p><input type="submit" name="sesion" value="Matar sesión"></p>
		</form>

</body>

<html>

<?php
//Tu solución aquí.

//Aunque eliminemos la sesion siempre mantenemos el array original de alumnos
if (!isset($_SESSION["alumnos"])) {
	$_SESSION["alumnos"] = array(
										array("nombre" => "Eugenio", "apellido" => "Martinez", "edad" => 45, "notas" => array(7, 6, 5, 8, 5, 6, 9, 10)),
										array("nombre" => "Marta", "apellido" => "Carrera", "edad" => 22, "notas" => array(1, 6, 2, 3, 5, 6, 9, 10, 10, 9)),
										array("nombre" => "Nacho", "apellido" => "Herrera", "edad" => 25, "notas" => array(3, 4, 2, 4, 6, 7, 9, 10, 3, 7)),
										array("nombre" => "Anxo", "apellido" => "Iglesias", "edad" => 32, "notas" => array(1, 6, 2, 3.2, 5, 2.2, 4.7, 5.5, 9, 9)),
										array("nombre" => "Valentina", "apellido" => "Iglesias", "edad" => 30, "notas" => array(9, 7))
									);
}

//Comprobamos que la validación ha sido correcta
if (isset($_SESSION["error"])) {
	echo $_SESSION["error"] . "<br>";
	unset($_SESSION["error"]);
}
else {
	//Pulsar boton enviar
	if (isset($_POST['enviar'])) {
		$nombre = $_POST['nombre'];
		$apellido = $_POST['apellido'];
		$edad = $_POST['edad'];
		$nota = $_POST['nota'];

		validacion($nombre, $apellido, $edad, $nota);
	}
}

//Pulsar botón matar sesion con el que eliminamos la sesión
if (isset($_POST['sesion'])){
	session_destroy();
	header("Location: e03.php");
}

eliminarVariablesSesionAlumnosNuevosYRepetidos();

function eliminarVariablesSesionAlumnosNuevosYRepetidos() {
	//Eliminamos variables de sesion de alumnos repetidos
	if (isset($_SESSION["nombreAlumnoRepetido"]) && isset($_SESSION["apellidoAlumnoRepetido"])) {
		unset($_SESSION["nombreAlumnoRepetido"]);
		unset($_SESSION["apellidoAlumnoRepetido"]);
	}
	//Eliminamos variables de sesion de alumnos nuevos
	if (isset($_SESSION["nombreAlumnoNuevo"]) && isset($_SESSION["apellidoAlumnoNuevo"])) {
		unset($_SESSION["nombreAlumnoNuevo"]);
		unset($_SESSION["apellidoAlumnoNuevo"]);
	}
}

//Función para gestionar los alumnos si el alumno no existe lo añade al array si existe añade la nota a su array
function gestionAlumnos($nombre, $apellido, $edad, $nota) {
	foreach ($_SESSION["alumnos"] as $alumno) {
		//Creamos variables de sesion en función de si el alumno introducido por formulario es un alumno nuevo o repetido
		if ($nombre === $alumno["nombre"] && $apellido === $alumno["apellido"]) {

			$_SESSION["nombreAlumnoRepetido"] = $alumno["nombre"];
			$_SESSION["apellidoAlumnoRepetido"] = $alumno["apellido"];
		}
		else {
			$_SESSION["nombreAlumnoNuevo"] = $alumno["nombre"];
			$_SESSION["apellidoAlumnoNuevo"] = $alumno["apellido"];
		}
	}
}

//Función que valida el formulario
function validacion($nombre, $apellido, $edad, $nota) {
	//Comprobamos que no quedo vacío ningún campo del formulario
	if (empty($nombre) || empty($apellido) || empty($edad) || empty($nota)) {
		$_SESSION["error"] = "ERROR. Hay algun campo vacio";
		header("Location: e03.php");
	}
	//Comprobamos caracteres de nombre
	else if(strlen($nombre) < 3 ) {
		$_SESSION["error"] = "ERROR. El nombre tiene que tener un minimo de 3 caracteres";
		header("Location: e03.php");
	}
	//Comprobamos caracteres de apellido
	else if(strlen($apellido) < 3 ) {
		$_SESSION["error"] = "ERROR. El apellido tiene que tener un minimo de 3 caracteres";
		header("Location: e03.php");
	}
	//Comprobamos que edad es int
	else if(!filter_var($edad, FILTER_VALIDATE_INT)) {echo "El alumno existe";
		$_SESSION["error"] = "ERROR. La edad tiene que ser un int";
		header("Location: e03.php");
	}
	//Comprobamos que nota es float
	else if(!filter_var($nota, FILTER_VALIDATE_FLOAT)) {
		$_SESSION["error"] = "ERROR. La nota tiene que ser un float";
		header("Location: e03.php");
	}
	//Comprobamos que la nota es mayor que 0 y menor que 10
	else if (($nota < 0) || ($nota > 10)) {
		$_SESSION["error"] = "ERROR. La nota tiene que ser mayor que 0 y menor que 10";
		header("Location: e03.php");
	}
	else {
		gestionAlumnos($nombre, $apellido, $edad, $nota);
		//Comprobamos si el alumno introducido por formulario es repetido o no, en caso de que sea repetido se le añade la nota al array de notas
		if (isset($_SESSION["nombreAlumnoRepetido"]) && isset($_SESSION["apellidoAlumnoRepetido"])) {

			foreach ($_SESSION["alumnos"] as $alumno) {
				if ($alumno["nombre"] == $_SESSION["nombreAlumnoRepetido"] && $alumno["apellido"] == $_SESSION["apellidoAlumnoRepetido"] ) {
					//Variable que guarda la posicion del alumno repetido en el array
					$indiceNombre = array_search($nombre, array_column($_SESSION["alumnos"], 'nombre'));

					$notaNueva = $nota;
					settype($notaNueva,"integer");

					array_push($_SESSION["alumnos"][$indiceNombre]["notas"], $notaNueva);
				}
			}
		}
		//Comprobamos si alumnos introducido por formulario es nuevo, si es asi, se añade al array de alumnos
		else if (isset($_SESSION["nombreAlumnoNuevo"]) && isset($_SESSION["apellidoAlumnoNuevo"])){
			settype($edad,"integer");
			settype($nota,"integer");
			$alumnoNuevo = array("nombre" => $nombre, "apellido" => $apellido, "edad" => $edad, "notas" => array($nota));
			array_push($_SESSION["alumnos"], $alumnoNuevo);
		}
	}
}

//EJERCICIO 2----------------------------------------------------------------------------------------------------------------------------
//Mostrar todos los alumnos y sus notas medias
echo "<h4>Todos los alumnos y sus notas medias:</h4>";
foreach ($_SESSION["alumnos"] as $alumno) {
	echo "Nombre: " . $alumno["nombre"] . " " . $alumno["apellido"] . ", Nota media: ";
	foreach ($alumno as $datos) {
		foreach ($datos as $nota) {
			$numNotas = count($datos);
			$sumnotas = $sumnotas + $nota;
			$media = $sumnotas/$numNotas;
		}
	}
	echo $media . "<br>";

	$numNotas = 0;
	$sumnotas = 0;
	$media = 0;
}

//Mostrar todos los alumnos y sus notas medias para estas edades[18,29]
echo "<h4>Todos los alumnos y sus notas medias para estas edades[18,29]:</h4>";
foreach ($_SESSION["alumnos"] as $alumno) {
	if ($alumno["edad"] >= 18 && $alumno["edad"]<= 29){
			echo "Nombre: " . $alumno["nombre"] . " " . $alumno["apellido"] . ", Nota media: ";
			foreach ($alumno as $datos) {
				foreach ($datos as $nota) {
					$numNotas = count($datos);
					$sumnotas = $sumnotas + $nota;
					$media = $sumnotas/$numNotas;
				}
			}
			echo $media . "<br>";

			$numNotas = 0;
			$sumnotas = 0;
			$media = 0;
		}
}

//Mostrar todos los alumnos y sus notas medias para estas edades[30,39]
echo "<h4>Todos los alumnos y sus notas medias para estas edades[30,39]:</h4>";
foreach ($_SESSION["alumnos"] as $alumno) {
	if ($alumno["edad"] >= 30 && $alumno["edad"]<= 39){
			echo "Nombre: " . $alumno["nombre"] . " " . $alumno["apellido"] . ", Nota media: ";
			foreach ($alumno as $datos) {
				foreach ($datos as $nota) {
					$numNotas = count($datos);
					$sumnotas = $sumnotas + $nota;
					$media = $sumnotas/$numNotas;
				}
			}
			echo $media . "<br>";

			$numNotas = 0;
			$sumnotas = 0;
			$media = 0;
		}
}

//Mostrar todos los alumnos y sus notas medias para estas edades[más de 40]
echo "<h4>Todos los alumnos y sus notas medias para estas edades[+40]:</h4>";
foreach ($_SESSION["alumnos"] as $alumno) {
	if ($alumno["edad"] > 40){
			echo "Nombre: " . $alumno["nombre"] . " " . $alumno["apellido"] . ", Nota media: ";
			foreach ($alumno as $datos) {
				foreach ($datos as $nota) {
					$numNotas = count($datos);
					$sumnotas = $sumnotas + $nota;
					$media = $sumnotas/$numNotas;
				}
			}
			echo $media . "<br>";

			$numNotas = 0;
			$sumnotas = 0;
			$media = 0;
		}
}

//Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos
echo "<h4>Lista de aptos:</h4>";
foreach ($_SESSION["alumnos"] as $alumno) {
	foreach ($alumno as $datos) {
		foreach ($datos as $nota) {
			$numNotas = count($datos);
			$sumnotas = $sumnotas + $nota;
			$media = $sumnotas/$numNotas;
		}
	}

	if ($numNotas >= 5 && $media >= 5) {
		echo $alumno["nombre"] . " " . $alumno["apellido"] . "<br>";
	}

	$numNotas = 0;
	$sumnotas = 0;
	$media = 0;
}

?>
