<html>

<head>
	<title>Ejercicio 01</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>

<body>

	<div id="enunciado" style="background-color: lightgray;">
		<h1>Enunciado</h1>
		<p>Esta primera tarea que os planteo se trata de generar, utilizando PHP, una página
		que contenga una división centrada, de 800px de altura, separada del margen superior
		por 50px y con background gris. En su interior deben ir listados, separados por comas,
		 los 5000 primeros números múltiplos de 3 o de 5.</p>

		<p>Ejemplo: <code>3,5,6,9,10...</code></p>

		<p>Es necesario que esta sea entregada en plazo para la
		evaluación</p>
	</div>

	<h1>Resultado</h1>

<?php

	echo "
		<div style='margin-top: 50px; background-color: gray; height: 800px; width: 80%; margin: 0px auto;font-size: 0.84em;'>
	 ";

		for ($i=0;$i<=5000;$i++) {
			if ( ($i%3==0) || ($i%5==0) ) {
				if ( $i == 5000) {
					echo $i;
				}
				else {
					echo $i . ", ";
				}
			}
		}

	echo "</div>";


?>

</body>

</html>
