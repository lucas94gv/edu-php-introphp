<html>

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
	<title>Ejercicio 02</title>
</head>

<body>
	<div id="enunciado" style="background-color: lightgray;">
	<h1>Enunciado</h1>
	<p>En un script php debes almacenar en una estructura de datos la siguiente información:</p>
	<ol>
		<li>Hay cinco alumnos que han sacado diferentes calificaciones a lo largo del curso en diferentes tareas asignadas:
			<ul>
				<li>Eugenio Martínez de 45 años - Notas de tareas: 7, 6, 5, 8, 5, 6, 9, 10</li>
				<li>Marta Carrera de 22 - Notas de tareas: 1, 6, 2, 3, 5, 6, 9, 10, 10, 9</li>
				<li>Nacho Herrera de 25 - Notas de tareas: 3, 4, 2, 4, 6, 7, 9, 10, 3, 7</li>
				<li>Anxo Iglesias de 32 - Notas de tareas: 1, 6, 2, 3'2, 5, 2'2, 4'7, 5'5, 9, 9</li>
				<li>Valentina Iglesias de 30 - Notas de tareas: 9, 7</li>
			</ul>
		</li>
		<li>La estructura de datos elegida es un array indexado por las claves nombre, apellido, edad, notas para cada alumno. Y un array que agrupe estos alumnos</li>
		<li>Es condición necesaria que toda esta estructura estea referenciada por una única variable para que pueda ser almacenada fácilmente en una variable de sesión</li>
		<li>Haz una lista que enumere los alumnos y sus notas medias.</li>
		<li>Calcula notas medias en función de los rangos de edad [18,29], [30,39], [más de 40]</li>
		<li>Haz una lista de aptos, teniendo en cuenta que la condición de APTO se da si tiene al menos 5 calificaciones y una media mínima de 5 puntos</li>
	</ol>

	<p>Es necesario que esta sea entregada en plazo para la evaluación</p>

	</div>

	<h1>Resultado</h1>
<?php
//Tu solución aquí

//Array en el que se almacena los alumnos
$alumnos = array(
								array("nombre" => "Eugenio", "apellido" => "Martínez", "edad" => 45, "notas" => array(7, 6, 5, 8, 5, 6, 9, 10)),
								array("nombre" => "Marta", "apellido" => "Carrera", "edad" => 22, "notas" => array(1, 6, 2, 3, 5, 6, 9, 10, 10, 9)),
								array("nombre" => "Nacho", "apellido" => "Herrera", "edad" => 25, "notas" => array(3, 4, 2, 4, 6, 7, 9, 10, 3, 7)),
								array("nombre" => "Anxo", "apellido" => "Iglesias", "edad" => 32, "notas" => array(1, 6, 2, 3.2, 5, 2.2, 4.7, 5.5, 9, 9)),
								array("nombre" => "Valentina", "apellido" => "Iglesias", "edad" => 30, "notas" => array(9, 7))
					);

//Recorremos el array y mostramos el nombre de cada alumno y su nota media------------------------------------------------------------------
foreach ($alumnos as $alumno) {
	echo "<b>Nombre:</b> " . $alumno["nombre"] . " <b>Nota media:</b> ";
		foreach ($alumno as $notas){
			foreach ($notas as $nota){
				$numNotas = count($notas);
				$sumnotas = $sumnotas + $nota;
				$media = $sumnotas/$numNotas;
			}
		}

	echo $media . "<br>";

	$numNotas = 0;
	$sumnotas = 0;
	$media = 0;
}

echo "<br><br><br>";

//Recorremos el array y mostramos la nota media en función de los rangos de edad------------------------------------------------------------------
foreach ($alumnos as $alumno) {
	//Rango de edad [18,29]
	if ($alumno["edad"] >= 18 && $alumno["edad"]<= 29) {
			foreach ($alumno as $notas){
				$numNotasAlumno = count($alumno["notas"]);
				foreach ($notas as $nota){
					$sumnotas1 = $sumnotas1 + $nota;
				}
			}
			$numNotas1 = $numNotas1 + $numNotasAlumno;
			$media1 = $sumnotas1/$numNotas1;
	}
	//Rango de edad [30,39]
	else if ($alumno["edad"] >= 30 && $alumno["edad"]<= 39) {
			foreach ($alumno as $notas){
				$numNotasAlumno2 = count($alumno["notas"]);
				foreach ($notas as $nota){
					$sumnotas2 = $sumnotas2 + $nota;
				}
			}
			$numNotas2 = $numNotas2 + $numNotasAlumno2;
			$media2 = $sumnotas2/$numNotas2;
	}
	//Rango de edad [más de 40]
	else if ($alumno["edad"] > 40) {
			foreach ($alumno as $notas){
				$numNotasAlumno3 = count($alumno["notas"]);
				foreach ($notas as $nota){
					$sumnotas3 = $sumnotas3 + $nota;
				}
			}
			$numNotas3 = $numNotas3 + $numNotasAlumno3;
			$media3 = $sumnotas3/$numNotas3;
	}
}

echo "<b>La media de notas para el rango de edad [18,29] es:</b> " . $media1 . "<br>";
echo "<b>La media de notas para el rango de edad [30,39] es:</b> " . $media2 . "<br>";
echo "<b>La media de notas para el rango de edad [más de 40] es:</b> " . $media3;

echo "<br><br><br>";

//Lista de aptos------------------------------------------------------------------------------------------------------------------------------------
foreach ($alumnos as $alumno) {
		foreach ($alumno as $notas){
			foreach ($notas as $nota){
				$totalNotas = count($notas);
				$sumnotas = $sumnotas + $nota;
				$media = $sumnotas/$totalNotas;
			}
		}

		if ($media > 5 && $totalNotas >= 5) {
			echo "<b>Nombre:</b> " . $alumno["nombre"] . "<b> Número de calificaciones:</b> " . $totalNotas . "<b> Media de notas:</b> " . $media . "<br>";
		}

		$media = 0;
		$totalNotas = 0;
		$sumnotas = 0;
}



?>

</body>

<html>
