<html>
<head>
<title>Exemplo de PHP</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>
<body>

<br>
<br>Primer ejemplo:
<br>----------------------------------------------------------------------------
<?php

	//Declaración en pantalla e impresión de la operación. ¿Podrías mejorarlo?
    $a = 8;
    echo "<p>a = 8</p>";
    $b = 3;
    echo "<p>b = 3</p>";
    
    
    if ($a < $b)
    {
    	//Esta instrucción se ejecutará siempre que a sea menor estricto que b
        echo "<p>'a' É menor que 'b'</p>";
    }
    
    if ($a == $b)
    {
    	//Esta instrucción se ejecutará si a es igual a b
        echo "<p>'a' É igual a 'b'</p>";
    }
    else
    {
    	//Esta instrucción se ejecutará si la condición del if que la precede, no se cumple
        echo "'a' É maior que 'b' o 'b' É maior que 'a'</p>";
    } 
?>

<br>
<br>Segundo ejemplo:
<br>----------------------------------------------------------------------------
<?php
    $a = 5;
    $b = 7;
    // Por exemplo
    if ($a>$b)
        $resultado= "A é máis grande que B";
    else
        $resultado= "B é máis grande que A";
    
    // Otra alternativa a la escritura de un if, con asignación de variable asociada. Importante entenderlo
    // Podería quedar así: 
    $resultado = ($a>$b) ? "A é máis grande que B":"B é máis grande que A";
    echo '<br>'.$resultado;
?>


<br>
<br>Tercer ejemplo:
<br>----------------------------------------------------------------------------
<?php
    
    $posicion = "arriba";
    
    switch($posicion) {
        case "arriba":   // Bloque 1
            echo "<p>A variable contén";
            echo " o valor arriba</p>";
            //El break interrumple la evaluación del resto de sentencias case. Importante entenderlo
            break;
        case "abaixo":   // Bloque 2
            echo "<p>A variable contén";
            echo " o valor abaixo</p>";
            break;
        default:   // Bloque 3
            echo "<p>A variable contén outro valor";
            echo " distinto de arriba e abaixo</p>";
    } 
    
?>

</body>
</html>