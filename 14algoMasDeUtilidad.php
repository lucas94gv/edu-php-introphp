<?php

//$_SERVER es una variable especial reservada por PHP que contiene toda la información del servidor web. Es conocida como una superglobal.

echo 'Para mostrar la variable $_SERVER, se puede hacer simplemente con un echo:<br><br>';
echo $_SERVER['HTTP_USER_AGENT'];

//Hay muchos tipos de variables en PHP. En el ejemplo anterior se muestra un
//elemento de un Array. Los arrays pueden ser muy útiles.

//$_SERVER es simplemente una variable que se encuentra disponible
//automáticamente en PHP. Se puede encontrar una lista en la sección Variables
//reservadas del manual

//Vamos a presentar un ejemplo útil que utiliza la evaluación de estas Variables
if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
    echo 'Está usando Internet Explorer.<br />';
}

//la función llamada a strpos(). strpos() es una función integrada en PHP que
//busca un string dentro de otro. En este caso estamos buscando 'MSIE' (también
//llamado aguja) dentro de $_SERVER['HTTP_USER_AGENT'] (también llamado pajar).
//Si el string se encuentra dentro del pajar, la función devuelve la posición
//de la aguja relativa al inicio del pajar. De lo contrario, devuelve FALSE. Si
//no devuelve FALSE, la expresión if se evalúa como TRUE y se ejecuta el código
//entre llaves {}. De lo contrario, el código no será ejecutado.

//Sobre impresiones (https://cybmeta.com/php-diferencias-entre-echo-print-print_r-y-var_dump)
//print es un constructor de lenguaje
echo '<pre>';
print 'Hola';
print PHP_EOL;
//La operación anterior es equivalente a
print('Hola'.PHP_EOL);
//print_r es una función, la siguiente expresión es incorrecta y generaría error
//print_r 'Hola';
echo '</pre>';

//Se define un array
$foo = array( 5, 0.0, "Hola", false, '' );

echo '<br>Impresión con var_dump:<pre>';
var_dump( $foo );
echo '</pre>';
echo '<br>Impresión con print_r:<pre>';
print_r( $foo );
echo '</pre>';
