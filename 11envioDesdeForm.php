<html>
<head>
	<title>Envio de datos PHP</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF8">
</head>
<body>
	<H1>Ejemplo de procesado de formularios</H1>

	 <?php

	 //Las variables $_POST y $_GET son establecidas automáticamente por php
	 //y almacenarán los datos enviados por los formularios dependiendo del método de envío GET o POST
	 	if($_GET['nombre']!=NULL and $_GET['apellidos']!=NULL){
	 		echo "Hola ".$_GET['nombre']." ".$_GET['apellidos']."!";
	 		imprimeBotonSalir();
	 	}
	 	else{
	 		if($_GET['nombre']!=NULL or $_GET['apellidos']!=NULL){
	 			echo '<div style="color:red; border:solid 1px;">Datos incompletos</div>';
			}
	 		imprimeFormulario();
	 	}

		function imprimeFormulario(){
			echo '<FORM ACTION="11envioDesdeForm.php" METHOD="GET">
					Introduce tu nombre:<INPUT TYPE="text" NAME="nombre"><BR>
					Introduce tus apellidos:<INPUT TYPE="text" NAME="apellidos"><BR>
					<INPUT TYPE="submit" VALUE="Enviar">
				</FORM> ';
		}

		function imprimeBotonSalir(){
			echo '<FORM ACTION="11envioDesdeForm.php" METHOD="GET">
					<INPUT TYPE="submit" VALUE="Volver">
				</FORM> ';
		}

		//Trata de mejorar este código para que si introduces algo Así
		//como '<p>Carolina</p>' dentro del input nombre, no se vea afectada la
		//visualización de la página
		//htmlspecialchars($_POST['nombre'])
	?>


</body>
</html>
